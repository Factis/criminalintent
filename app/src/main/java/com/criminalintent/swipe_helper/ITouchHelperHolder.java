package com.criminalintent.swipe_helper;

public interface ITouchHelperHolder {

    void onItemSelected();

    void onItemClear();
}
