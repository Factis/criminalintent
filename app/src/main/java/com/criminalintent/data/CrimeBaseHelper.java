package com.criminalintent.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.criminalintent.data.CrimeDbSchema.CrimeTable;

public class CrimeBaseHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "crimeBase.db";

    public CrimeBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder builder = new StringBuilder();
        builder.append("create table ");
        builder.append(CrimeTable.NAME);
        builder.append("(");
        builder.append(" _id integer primary key autoincrement, ");
        builder.append(CrimeTable.Cols.UUID);
        builder.append(", ");
        builder.append(CrimeTable.Cols.TITLE);
        builder.append(", ");
        builder.append(CrimeTable.Cols.DATE);
        builder.append(", ");
        builder.append(CrimeTable.Cols.SOLVED);
        builder.append(", ");
        builder.append(CrimeTable.Cols.REQUIRES_POLICE);
        builder.append(", ");
        builder.append(CrimeTable.Cols.SUSPECT);
        builder.append(")");

        db.execSQL(builder.toString());

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
