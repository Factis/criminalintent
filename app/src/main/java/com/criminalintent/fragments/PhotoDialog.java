package com.criminalintent.fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.criminalintent.PictureUtils;
import com.criminalintent.R;

import java.io.File;

public class PhotoDialog extends DialogFragment {

    private static final String ARG_PATH = "photo_path";

    public static PhotoDialog newInstance(String path) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_PATH, path);

        PhotoDialog photoDialog = new PhotoDialog();
        photoDialog.setArguments(args);
        return photoDialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        String path = (String) getArguments().getSerializable(ARG_PATH);
        File file = new File(path);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_photo, null);
        ImageView photoView = view.findViewById(R.id.crime_photo_dialog);

        if (file.exists()) {
            Bitmap bitmap = PictureUtils.getScaledBitmap(path, getActivity());
            photoView.setImageBitmap(bitmap);
        }

        Dialog dialog = new Dialog(getContext(), R.style.MyDialog);
        dialog.requestWindowFeature(Window.FEATURE_SWIPE_TO_DISMISS);
        dialog.setContentView(view);
        return dialog;
    }
}
