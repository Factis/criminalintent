package com.criminalintent.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.*;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.*;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.*;
import android.widget.*;

import com.criminalintent.R;
import com.criminalintent.models.Crime;
import com.criminalintent.models.CrimeLab;
import com.criminalintent.swipe_helper.ITouchHelperAdapter;
import com.criminalintent.swipe_helper.ITouchHelperHolder;
import com.criminalintent.swipe_helper.TouchHelperCallback;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class CrimeListFragment extends Fragment {

    private static final String SAVED_SUBTITLE_VISIBLE = "subtitle";

    private RecyclerView crimeRecyclerView;
    private TextView emptyTextView;
    private CrimeAdapter adapter;
    private boolean subtitleVisible;
    private Callbacks callbacks;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callbacks = (Callbacks) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_crime_list, container, false);
        emptyTextView = view.findViewById(R.id.list_is_empty);
        crimeRecyclerView = view.findViewById(R.id.crime_recycler_view);
        crimeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));



        if (savedInstanceState != null) {
            subtitleVisible = savedInstanceState.getBoolean(SAVED_SUBTITLE_VISIBLE);
        }
        updateUI();

        ItemTouchHelper.Callback callback = new TouchHelperCallback(adapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(crimeRecyclerView);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SAVED_SUBTITLE_VISIBLE, subtitleVisible);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void checkList(List<Crime> crimes) {
        if (crimes.isEmpty()) {
            emptyTextView.setVisibility(View.VISIBLE);
        } else {
            emptyTextView.setVisibility(View.GONE);
        }
    }

    public void updateUI() {
        CrimeLab crimeLab = CrimeLab.get(getActivity());
        List<Crime> crimes = crimeLab.getCrimes();
        checkList(crimes);

        if (adapter == null) {
            adapter = new CrimeAdapter(crimes);
            crimeRecyclerView.setAdapter(adapter);
        } else {
            adapter.setCrimes(crimes);
            adapter.notifyDataSetChanged();
        }
        updateSubtitle();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_crime_list, menu);

        MenuItem subtitleItem = menu.findItem(R.id.show_subtitle);
        if (subtitleVisible) {
            subtitleItem.setTitle(R.string.hide_subtitle);
        } else {
            subtitleItem.setTitle(R.string.show_subtitle);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_crime:
                Crime crime = new Crime();
                CrimeLab.get(getActivity()).addCrime(crime);
                updateUI();
                callbacks.onCrimeSelected(crime);
                return true;
            case R.id.show_subtitle:
                subtitleVisible = !subtitleVisible;
                getActivity().invalidateOptionsMenu();
                updateSubtitle();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSubtitle() {
        CrimeLab crimeLab = CrimeLab.get(getActivity());
        int crimeCount = crimeLab.getCrimes().size();
        String subtitle = getResources().getQuantityString(R.plurals.subtitle_plural, crimeCount, crimeCount);

        if (!subtitleVisible) {
            subtitle = null;
        }

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.getSupportActionBar().setSubtitle(subtitle);
    }

    private void updateDatabase(final List<Crime> crimes) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                CrimeLab crimeLab = CrimeLab.get(getActivity());
                crimeLab.updateDB(crimes);
            }
        }).start();
    }

    public interface Callbacks {
        void onCrimeSelected(Crime crime);
    }

    private class CrimeHolder extends RecyclerView.ViewHolder implements View.OnClickListener, ITouchHelperHolder {
        private TextView titleTextView;
        private TextView dateTextView;
        private ImageView solvedImageView;
        private Button requiresPolice;
        private Crime crime;

        CrimeHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_crime, parent, false));
            titleTextView = itemView.findViewById(R.id.crime_title);
            dateTextView = itemView.findViewById(R.id.crime_date);
            solvedImageView = itemView.findViewById(R.id.crime_solved);
            requiresPolice = itemView.findViewById(R.id.crime_requires_police);
            itemView.setOnClickListener(this);
        }

        void bind(Crime crime) {
            this.crime = crime;
            String date = new SimpleDateFormat("EEEE, dd.MM.yyy", Locale.ROOT).format(crime.getDate());
            titleTextView.setText(crime.getTitle());
            dateTextView.setText(date);
            solvedImageView.setVisibility(crime.isSolved() ? View.VISIBLE : View.GONE);
            requiresPolice.setVisibility(crime.isRequiresPolice() ? View.VISIBLE : View.GONE);
        }

        @Override
        public void onClick(View v) {
            callbacks.onCrimeSelected(crime);
        }

        @Override
        public void onItemSelected() {
            Log.d("TAG", "onItemSelected: ");
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            Log.d("TAG", "onItemClear: ");
            itemView.setBackgroundColor(0);
        }
    }

    private class CrimeAdapter extends RecyclerView.Adapter<CrimeHolder> implements ITouchHelperAdapter {
        private List<Crime> crimes;

        CrimeAdapter(List<Crime> crimes) {
            this.crimes = crimes;
        }

        @NonNull
        @Override
        public CrimeHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            return new CrimeHolder(inflater, viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull CrimeHolder holder, int i) {
            Crime crime = crimes.get(i);
            holder.bind(crime);
        }

        @Override
        public int getItemCount() {
            return crimes.size();
        }



        void setCrimes(List<Crime> crimes) {
            this.crimes = crimes;
        }

        @Override
        public void onItemMove(int fromPosition, int toPosition) {
            Log.d("TAG", "onItemMove: ");

            Collections.swap(crimes, fromPosition, toPosition);
            notifyItemMoved(fromPosition, toPosition);

            updateDatabase(crimes);
        }



        @Override
        public void onItemDismiss(int position) {
            Log.d("TAG", "onItemDismiss: ");
            CrimeLab crimeLab = CrimeLab.get(getActivity());
            crimeLab.deleteCrime(crimes.get(position).getId());
            notifyItemRemoved(position);
            updateUI();
        }
    }
}
